# Laravel Docker Blueprint


## Installation

1. Copy all files (except this readme) to laravel root directory ( where app and storage is )
2. docker-compose up -d
3. In configuration use "database" instead of 127.0.0.1 or localhost
4. docker-compose up -d

## Info

* MySQL - root / mysql => laravel
* Redis - redispassword

## Known bugs

1. On some devices ( especialy where laravel wallet was ) change MySQL port to another on then 3306
   ```php
   - 3306:3306 => - 3307:3306
   ```